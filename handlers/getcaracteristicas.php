<?php

//error_reporting(E_ALL);

include '../includes/functions.php';
header('Content-Type: text/plain');

$id = "";

$id = $_POST['id'];
        
try{
    
    $objF = new Funciones();
    $result = $objF->get_caracteristicas($id);
        
    
    if($result != null){
        
        $total = $result->rowCount();
        $actual = 0;
        
        $price = $objF->get_data_price($id);
        
        
        echo ''
        . '{"success": "true", '
        . '"price": "' . split_price($price["price"]) . '", '
        . '"price_s": "' .($price["price"]) . '", '
        . '"items":[';
        
        foreach($result as $item){
            echo "{"
                . '"id_product":"'. $item["id_product"] . '",'
                . '"titulo":"'. str_replace(PHP_EOL, '', $item["titulo"]) . '",'
                . '"text":"'. str_replace(PHP_EOL, '', $item["text"])  . '"'
            . "}";
            
            $actual ++;
            if($actual < $total){
                echo ',';
            }
        }
        echo ']'
        
        
                . '}';
    }
    else {
        echo '{"success": "false", "exists": "true"}';
    }
            
       
    
        
        
} catch (Exception $e) {
    echo '{"success": "false", "mess": "$e"}';
}





function getGUID(){
    if (function_exists('com_create_guid')){
        return com_create_guid();
    }else{
        mt_srand((double)microtime()*10000);//optional for php 4.2.0 and up.
        $charid = strtoupper(md5(uniqid(rand(), true)));
        $hyphen = chr(45);// "-"
        $uuid = "" //chr(123)// "{"
            .substr($charid, 0, 8).$hyphen
            .substr($charid, 8, 4).$hyphen
            .substr($charid,12, 4).$hyphen
            .substr($charid,16, 4).$hyphen
            .substr($charid,20,12)
            .""; //.chr(125);// "}"
        return $uuid;
    }
}
    
    
//}

?>